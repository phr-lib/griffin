<?php


namespace Phr\Griffin\Service;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Griffin\GriffinException;

use Phr\Griffin\Session\ActiveSession;
use Phr\Griffin\Contracts\Login\AccountTokenResponse;
use Phr\Griffin\Service\ServerService;

use Phr\Griffin\Tokens\SecureAccountJwt;
use Phr\Griffin\Tokens\Settings;
use Phr\Eojwt\EoJwtBase\TokenType;

use Phr\Eojwt\EoJwtException;

class TokenService extends GriffinServiceModel
{   
    readonly private ServerService $serverService;

    public function __construct(readonly private ActiveSession $session)
    {
       $this->serverService = new ServerService;
    }

    public function userInfo(): AccountTokenResponse
    {   
        return $this->session->userInfoToken();
    }
    public function serverToken()
    {   
        $serverId = SHELL::parameters();
        if($serverId == null) throw new GriffinException(ERR::E5655000, 'No server id');
        $serverData = $this->serverService->getServerById($serverId);
        return $this->session->serverToken($serverData);
    }
    
    
}
