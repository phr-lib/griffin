<?php


namespace Phr\Griffin\Service;

use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\GriffinBase\Errors as ERR;

use Phr\Webapi\Utility\Entity\Sessions;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\Entity\Users;
use Phr\Sqlbridge\SqlException;
use Phr\Griffin\GriffinException;
use Phr\Griffin\Contracts\UserResponse;


use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Certificator\Crips;
use Phr\Certificator\Encry\RsaKeyGenerator;
use Phr\Certificator\Encryption;
use Phr\Griffin\Session\SessionBase\UserSession;

class SessionService extends GriffinServiceModel
{   
    
    public function getSessionByUserId(string $_user_id): array
    {   
        try
        {   
            $result = self::$sql->query("SELECT * FROM Sessions
            JOIN Users ON Sessions.userId=Users.uuid
            WHERE userId='$_user_id'");
            $results = mysqli_fetch_all($result, MYSQLI_ASSOC);
            $result->free_result();
            
            return $results;

        }catch(SqlException $error)
        {
            throw new GriffinException(ERR::E5655000);
        }
        
    }

    public function getSessionByUserIdBACK(string $_user_id): Sessions
    {   
        try
        {   
            $result = $this->fetch(Sessions::class, ['userId', $_user_id]);
            return self::entity($result[0]);            

        }catch(SqlException $error)
        {
            throw new GriffinException(RC::OK, ERR::E5655000, $error->getMessage().$error->getCode());
        }
        
    }
    public function createNewUserSession(UserSession $_user_session)
    {   
        try
        {   
            $exists = $this->fetch(Sessions::class, ['userId', $_user_session->userId]);
            if($exists)
            {
                $this->update(new Sessions(
                    $_user_session->userId,
                    $_user_session->sessionId,
                    $_user_session->sessionTs,
                    $_user_session->expire,
                    $_user_session->enryptor,
                    546,
                    $_user_session->sessionToken()
                ), ['userId', $_user_session->userId]);
            }else
            {
                $this->insert(new Sessions(
                    $_user_session->userId,
                    $_user_session->sessionId,
                    $_user_session->sessionTs,
                    $_user_session->expire,
                    $_user_session->enryptor,
                    546,
                    $_user_session->sessionToken()
                ));
            }

        }catch(SqlException $error)
        {
            throw new GriffinException(ERR::E5655000, "session insert");
        }
        
    }
    private static function entity(array $data): Sessions
    {
        return(new Sessions(
            $data['userId'],
            $data['sessionId'],
            $data['sessionTs'],
            $data['expire'],
            $data['enryptor'],
            $data['sessionIv'],
            $data['token']
        ));
    }
   
    
    
    
}
