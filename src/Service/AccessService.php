<?php


namespace Phr\Griffin\Service;

use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\Entity\ClientAccess;
use Phr\Griffin\Contracts\Access\ClientAccessResponse;
use Phr\Griffin\Contracts\Access\ClientAccessRequest;
use Phr\Sqlbridge\SqlException;
use Phr\Griffin\Entity\Server;
use Phr\Griffin\Contracts\Realm\ServerStatus;
use Phr\Webapi\ApiTools\Entity;
use Phr\Griffin\Contracts\Access\UserRolRequest;
use Phr\Griffin\Entity\UserRols;

class AccessService extends GriffinServiceModel
{
    public function getAccess(string $serverId): array
    {
        return $this->fetchResponse(ClientAccess::class, ClientAccessResponse::class, ['serverId', $serverId]);
    }
    public function newAccess(ClientAccessRequest $request): string
    {
        try{
            $this->insert($request->entity());
            return (string)"Client access added";
        }
        catch(SqlException $error){throw new GriffinException(RC::INTERNAL_SERVER_ERROR, ERR::E5655000, "PASSWORD");}
    }
    public function getServers(string $params): array
    {   
        $search = ['realmId', $params];
        $result = $this->fetchResponse(Server::class, ServerStatus::class, $search);
        return $result;
    }
    public function deleteUserAccess(string $userId): string
    {
        try{
            
            $result = self::$sql->delete(Entity::getTable(ClientAccess::class), ['userId', $userId]);
            return $result ? "User removed from database":"error";
                  
        }catch(SqlException $error)
        {   
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010);
        }
    }
    public function getAllUserRols()
    {

    }
    public function addNewAccessRole(UserRolRequest $request)
    {
        try{
            $this->insert($request->entity());
            return (string)"User role added";
        }
        catch(SqlException $error){throw new GriffinException(RC::INTERNAL_SERVER_ERROR, ERR::E5655000);}
    }
    public function deleteAccessRole(string $role)
    {   
        try{
            $result = self::$sql->delete(Entity::getTable(UserRols::class), ['role', $role]);
            return $result ? "User removed from database":"error";
        }
        catch(SqlException $error){throw new GriffinException(RC::INTERNAL_SERVER_ERROR, ERR::E5655000);}
    }
}