<?php


namespace Phr\Griffin\Service;

use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\Entity\Realm;
use Phr\Sqlbridge\SqlException;
use Phr\Griffin\GriffinException;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Griffin\Contracts\Realm\RealmResponse;
use Phr\Certificator\Crips;
use Phr\Certificator\Encry\RsaKeyGenerator;
use Phr\Griffin\Contracts\Realm\RealmPublic;
use Phr\Griffin\Entity\Users;
use Phr\Griffin\Contracts\User\UserResponse;
use Phr\Griffin\Contracts\Realm\NewServerRequest;
use Phr\Griffin\Entity\Server;
use Phr\Griffin\Contracts\Realm\ServerStatus;

class RealmService extends GriffinServiceModel
{   
    
    public function createNewRealm()
    {   
        try
        {   
            $realmId = Crips::newGuid();
            $registrationId = Crips::newGuid();

            $rsaGenerator = new RsaKeyGenerator;
            $rsaGenerator->createKeyPairs();

            $this->insert(new Realm(
                $realmId
                ,$registrationId 
                ,$rsaGenerator->publicKey()
                ,$rsaGenerator->privateKey()
            ));

        }catch(SqlException $error)
        {
            throw new GriffinException(RC::OK, ERR::E5655000, $error->getMessage().$error->getCode());
        }
        
    }
    public function contract(): array
    {   
        return $this->fetchResponse(Realm::class, RealmResponse::class);
    }
    public function registerServer(NewServerRequest $request): string
    {
        try{$this->insert($request->toEntity()); return "Client registered";
        }catch(SqlException $error){throw new GriffinException(RC::OK, ERR::E5655000, $error->getMessage().$error->getCode());}      
    }
    public function serverStatus(string $params): ServerStatus|null
    {   
        if($params != 'server') $search = ['serverId', $params];else $search = null;
        $result = $this->fetchResponse(Server::class, ServerStatus::class, $search);
        if(isset($result[0])) return $result[0]; else return null;
    }
    
    /**
     * @method fetch for realm
     * @param string realm id
     * @return Realm
     */
    public function getRealmById(string $realmId): Realm
    {
        try{
            $result = $this->fetch(Realm::class, ['realmId', $realmId]);
            if($result == null) throw new GriffinException(ERR::E5655000, 'No realm');
            else return self::fillClass($result[0]);
        }catch(SqlException $error){throw new GriffinException(RC::OK, ERR::E5655000, $error->getMessage().$error->getCode());}
    }
    public function realmPrivate(string $realmId): string
    {   
        $realmData = $this->getRealmById($realmId);
        return $realmData->privatePem;
    }
    private static function fillClass($result): Realm
    {   
        return(new Realm(
            $result['realmId']
            ,$result['registrationId']
            ,$result['publicPem']
            ,$result['privatePem']               
        ));
    }



    public function realmKey(): RealmPublic
    {   
        $realmData = $this->fetchAll();
        return(new RealmPublic($realmData[0]->publicPem));
    }
    
    public function realmPublic(): string
    {   
        $realmData = $this->fetchAll();
        return $realmData[0]->publicPem;
    }
    
    public function fetchAll(): array
    {   
        $result = $this->fetch(Realm::class);
        
        return array_map('self::fillClass', $result);

    }
    
}
