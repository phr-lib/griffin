<?php


namespace Phr\Griffin\Service;

use Phr\Griffin\GriffinException;
use Phr\Webapi\ApiBase\ApiShell;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Webapi\ApiBase\Support\FileContent\ConfigFile;
use Phr\Sqlbridge\SqlException;

use Phr\Certificator\SaveFile;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\FileHandler\FileSettings;
use Phr\Certificator\Crips;
use Phr\Certificator\Encryption;
use Phr\Certificator\Encry\RsaKeyGenerator;
use Phr\Certificator\FileHandler\AplContent;
use Phr\Certificator\FileHandler\Line\AplLine;
use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Certificator\FileHandler\Line\ConfigLine;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\Session\MasterSessionControl;

use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\Entity\Certs\MasterCert;
use Phr\Griffin\GriffinBase\Support\SupportFiles;
use Phr\Griffin\Contracts\User\MasterUserRequest;
use Phr\Griffin\Entity\Users;
use Phr\Griffin\Contracts\UsersResponse;
use Phr\Griffin\Contracts\User\MasterLoginRequest;
use Phr\Griffin\Contracts\Login\LoginResponse;
use Phr\Griffin\Contracts\User\NewUserRequest;
use Phr\Griffin\Contracts\User\UserResponse;

use Phr\Eojwt\EoJwt;
use Phr\Eojwt\EoJwtException;
use Phr\Griffin\Contracts\Login\AccountTokenResponse;



class AdminService extends GriffinServiceModel
{   
    readonly private MasterSessionControl $session;

    public function __construct()
    {   
        parent::__construct();
        $this->session = new MasterSessionControl;
    }
    /**
     * @method logs in master user
     * @param MasterLoginRequest
     * @return LoginResponse
     */
    public function masterLogin(MasterLoginRequest $request): LoginResponse
    {   
        $username = $request->username;
        $password = $request->password;
        /**
         * Retrive master username and 
         * password form master certificate!
         */
        $masterData = SHELL::retriveGriffinFile(SupportFiles::IDP_MASTER_CERT, SHELL::masterEncryptor());
        
        if($masterData->content->username === $username)
        {
            if($masterData->content->password === $password)
            {   
                /**
                 * Ceate new master cert session!
                 */
                $session = new MasterSessionControl;
                /**
                 * Return master refresh token
                 */
                return $session->createNewMasterSession($username);

            }else throw new GriffinException(RC::INTERNAL_SERVER_ERROR, ERR::E5655000, "PASSWORD");
        }else throw new GriffinException(RC::INTERNAL_SERVER_ERROR, ERR::E5655000, "USERNAME");
    }
    public function masterToken(): AccountTokenResponse
    {   
        $this->session->validate();
        return $this->session->masterInfoToken();   
    }
    
    /**
     * 
     * @method create new master user
     * @param MasterUserRequest
     * @return string to phr response header
     * 
     */
    public function createNewMasterUser(MasterUserRequest $request): string
    {   
        $username = $request->username;
        $password = $request->password;
       
        try
        {   
            $regDate = new \DateTime('now');
            $confData = ApiShell::retriveFile(SF::REGISTRATION_CONFIG_FILE);
            $registrationId = $confData[ConfigFile::REG_ID->value];

            $certificate = new SaveFile(ApiShell::projectFolder().SupportFiles::IDP_MASTER_CERT->folder(), 
            new FileSettings(   SupportFiles::IDP_MASTER_CERT->fileType()
                                ,ApiShell::settings()->solutionId
                                ,ApiShell::settings()->applicationId
                                ,SHELL::masterEncryptor()
                            )
            );

            $certificate->createCertificate(new MasterCert($username, $password), SupportFiles::IDP_MASTER_CERT->fileName());

            $masterApl = new SaveFile(ApiShell::syssFolder().SupportFiles::MASTER_APL->folder(), 
            new FileSettings(   SupportFiles::MASTER_APL->fileType()
                                ,ApiShell::settings()->solutionId
                                ,ApiShell::settings()->applicationId
                                ,null
                            )
            );
            $aplContent = new AplContent("Master registration",new AplLine(["Date", $regDate->format(SupportFiles::MASTER_APL->dateFormat())], "register"));
            $aplContent->add(new AplLine(["RegistrationId", $registrationId], "Registration"));
            $masterApl->create($aplContent, SupportFiles::MASTER_APL->fileName());

            $masterConfig = new SaveFile(ApiShell::projectFolder().SupportFiles::MASTER_CERT->folder(), 
            new FileSettings(   SupportFiles::MASTER_CERT->fileType()
                                ,ApiShell::settings()->solutionId
                                ,ApiShell::settings()->applicationId
                                ,null
                            )
            );
            $masterConfigContent = new ConfigContent(new ConfigLine(ConfigFile::DATE->value, SupportFiles::MASTER_APL->dateFormat()));
            $masterConfigContent->add(new ConfigLine("RegId", $registrationId));

            $masterConfig->create($masterConfigContent, SupportFiles::MASTER_CERT->fileName());
            
            return "Master user created!!!";

        }catch(SaveFileError $error){}
    }
    public function createNewUser(NewUserRequest $request): string
    {   
        
        try
        {  
            $realmId = $request->realmId;
            $username = $request->username;
            $password = $request->password;
            
            $this->insert(new Users($realmId
                ,$username
                ,$password
                ,'notyetset'
                ,'notyetset'

            ));
            return "User ".$username." created in realm ".$realmId;


        }catch(SqlException $error)
        {
            throw new GriffinException(RC::OK, ERR::E5655000, $error->getMessage().$error->getCode());
        }
        
    }

    public function getAllUsersInRealm(string $realmId)
    {
        return $this->fetchResponse(Users::class, UserResponse::class, ['realmId', $realmId]);
    }
 
    public function usersResponse(): UsersResponse
    {   
        $result = $this->fetch(Users::class);
        $response = new UsersResponse;
        foreach($result as $user)
        {
            $response->add(new UserResponse(
                $user['uuid']    
            ));
        }
        
        return $response;

    }
    
}
