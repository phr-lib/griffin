<?php


namespace Phr\Griffin\Service;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\Entity\Users;
use Phr\Sqlbridge\SqlException;
use Phr\Griffin\GriffinException;
use Phr\Griffin\Contracts\UserResponse;


use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Certificator\Crips;
use Phr\Certificator\Encry\RsaKeyGenerator;
use Phr\Certificator\Encryption;

class UserService extends GriffinServiceModel
{   
    
    public function createNewClient()
    {   
        try
        {   
            
            $posted = SHELL::posted();
            $clientKey = $posted->clientKey;
 
            $clientKey = Encryption::decodeHex($clientKey);
        
            $applicationId = $posted->applicationId;
            $realmId = $posted->realmId;
            
            $realmId = $realmId;
            $applicationId = $applicationId;

            #$rsaGenerator = new RsaKeyGenerator;
            #$rsaGenerator->createKeyPairs();

            $this->insert(new Client(
                $realmId
                ,$applicationId 
                ,$clientKey
            ));

        }catch(SqlException $error)
        {
            throw new GriffinException(RC::OK, ERR::E5655000, $error->getMessage().$error->getCode());
        }
        
    }
    public function getUserIdByUsername(string $username): string|null
    {   
        $result = $this->fetch(Users::class, ['username', $username]);
        if($result == null) return null;
        return $result[0]['uuid'];
    }
    public function getUserByUsername(string $username): Users|null
    {   
        $result = $this->fetch(Users::class, ['username', $username]);
        if($result == null) return null;
        return self::entity($result[0]);
    }
    public function getUserById(string $username): UserResponse
    {   
        $result = $this->fetch(Users::class, ['uuid', $username]);
        $result = $result[0];
        return(new UserResponse(
            $result['uuid'],
            $result['uuid'],
            $result['uuid'],
            $result['uuid'],
            $result['uuid'],

                    
        ));
    }
    private static function entity(array $data): Users
    {
        return (new Users(
            $data['realmId'],
            $data['username'],
            $data['password'],
            $data['publicPem'],
            $data['privatePem']
        ));
    }
    
    
    
}
