<?php


namespace Phr\Griffin\Service;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\GriffinException;
use Phr\Griffin\GriffinBase\Errors as ERR;

use Phr\Griffin\Session\SessionControl;
use Phr\Griffin\Service\RealmService;
use Phr\Griffin\Service\UserService;

use Phr\Webapi\ApiBase\ApiShell;
use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\Entity\Users;
use Phr\Sqlbridge\SqlException;
use Phr\Griffin\Contracts\ServiceResponse;

use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Certificator\Crips;
use Phr\Certificator\Encry\RsaKeyGenerator;
use Phr\Certificator\Encryption;
use Phr\Griffin\Contracts\Login\LoginResponse;
use Phr\Griffin\Contracts\User\UserLoginRequest;

/**
 * 
 * Login service 
 * 
 * 
 */
class LoginService extends GriffinServiceModel
{   
    readonly private RealmService $realmService;

    readonly private UserService $userService;

    # /// CONSTRUCTOR ***
    public function __construct()
    {
        parent::__construct();
        $this->realmService = new RealmService;
        $this->userService = new UserService;
    }
    /**
     * @method gets realm public rsa token
     * @return ServiceResponse
     */
    public function serviceProtocol(): ServiceResponse
    {   
        $realmId = SHELL::parameters();
        if(!$realmId) throw new GriffinException(ERR::E5010000, 'RealmId');
        /**
         * Fetch public realm rsa for
         * selected realm.
         */
        $data = $this->realmService->getRealmById($realmId);
        $encodedPublic = Encryption::encodeHex($data->publicPem);
        return new ServiceResponse($encodedPublic);
    }
    public function createNewClient()
    {   
        try
        {  
            

        }catch(SqlException $error)
        {
            throw new GriffinException(RC::OK, ERR::E5655000, $error->getMessage().$error->getCode());
        }
        
    }
    public function getUserById(string $userId): UserResponse
    {   
        $result = $this->fetch(Users::class, " uuid='{$userId}'");
        $result = $result[0];
        return(new UserResponse(
            $result['uuid']
                    
        ));
    }

    public function userLogin(): LoginResponse
    {   
        $realmId = SHELL::parameters();
        if(!$realmId) throw new GriffinException(ERR::E5010000, 'RealmId');
        $realmData = $this->realmService->getRealmById($realmId);
        if(!$realmData->privatePem) throw new GriffinException(ERR::E5010000, 'Private pem');
        $userRequest = new UserLoginRequest(SessionControl::digestRealmEnryption($realmData->privatePem));
        
        $userId = $this->userService->getUserIdByUsername($userRequest->username);
        if($userId == null) throw new GriffinException(ERR::E5010000, 'Username does not exists');
        
        $session = new SessionControl(
            $userId,
            $realmId,
            "clientId"
        );
        return $session->createNewUserSession($userRequest->username, $realmData->publicPem);

    }
    
    
    
}
