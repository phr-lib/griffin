<?php


namespace Phr\Griffin\Service;

use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\Entity\Server;
use Phr\Sqlbridge\SqlException;


class ServerService extends GriffinServiceModel
{
    public function getServerById(string $serverId): Server
    {   
        try{
            $result = $this->fetch(Server::class, ['serverId', $serverId]);
            if(!$result) new GriffinException(ERR::E5655000, "get server");
            return Server::entity($result[0]);
        }catch(SqlException $error){throw new GriffinException(RC::INTERNAL_SERVER_ERROR, ERR::E5655000, "PASSWORD");}
        
    }
}