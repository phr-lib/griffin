<?php


namespace Phr\Griffin\Service;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Griffin\Service\GriffinServiceModel;
use Phr\Griffin\Entity\Client;
use Phr\Sqlbridge\SqlException;
use Phr\Griffin\GriffinException;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Griffin\Contracts\RealmResponse;
use Phr\Certificator\Crips;
use Phr\Certificator\Encry\RsaKeyGenerator;
use Phr\Certificator\Encryption;

class ClientService extends GriffinServiceModel
{   
    
    public function createNewClient()
    {   
        try
        {  
            $posted = SHELL::posted();
            $clientKey = $posted->clientKey;
 
            $clientKey = Encryption::decodeHex($clientKey);
        
            $applicationId = $posted->applicationId;
            $realmId = $posted->realmId;
            
            $realmId = $realmId;
            $applicationId = $applicationId;

            $rsaGenerator = new RsaKeyGenerator;
            $rsaGenerator->createKeyPairs();

            $this->insert(new Client(
                $realmId
                ,$applicationId 
                ,$clientKey
            ));

        }catch(SqlException $error)
        {
            throw new GriffinException(RC::OK, ERR::E5655000, $error->getMessage().$error->getCode());
        }
        
    }
    
    public function contract(): RealmResponse
    {   
        return(new RealmResponse($this->fetchAll()));
    }
    public function fetchAll(): array
    {   
        $result = $this->fetch(Realm::class);
        
        return array_map('self::fillClass', $result);

    }
    private static function fillClass($result): Realm
    {   
        return(new Realm(
            $result['realmId']
            ,$result['registrationId']
            ,$result['publicPem']
            ,$result['privatePem']               
        ));

    }
}
