<?php


namespace Phr\Griffin\Controllers;

use Phr\Webapi\ApiControl\ApiController;
use Phr\Shell\Http\IHttpMethod;
use Phr\Shell\Shell;
use Phr\Griffin\Service\RealmService;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiControl\Response;
use Phr\Webapi\ApiControl\Contracts\ListResponse;
use Phr\Griffin\Contracts\Realm\NewServerRequest;
use Phr\Griffin\Contracts\Security\PublicRealmResponse;

final class RealmController extends ApiController
{   
    readonly private RealmService $service;

    public function __construct()
    {
        $this->service = new RealmService;
    }
    public function controller(): void
    {   
        switch ( Shell::route(2) ) 
        {   
            case 'server': $this->serverController(); break;
            case 'public': $this->methodPublicRealm(); break;
            default: $this->methodController(); break;
        }
    }
    public function methodController(): void
    {   
        
        switch ( self::method() ) 
        {
            case IHttpMethod::GET: Response::Ok(new ListResponse($this->service->contract())); break;

            case IHttpMethod::POST: $service->createNewRealm(); Response::Ok(new ListResponse()); break;

            
            default:
                # code...
                break;
        }
    }
    public function serverController(): void
    {   
        
        switch ( self::method() ) 
        {   
            case IHttpMethod::GET: Response::OK($this->service->serverStatus(self::parameters())); break;
            case IHttpMethod::POST: Response::Accepted($this->service->registerServer(new NewServerRequest(self::posted()))); break;

        }
    }
    public function methodPublicRealm(): void
    {   
        
        switch ( self::method() ) 
        {   
            case IHttpMethod::GET: Response::OK(new PublicRealmResponse); break;
        }
    }
}