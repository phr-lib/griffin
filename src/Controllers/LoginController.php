<?php


namespace Phr\Griffin\Controllers;


use Phr\Webapi\ApiBase\ApiShell;
use Phr\Webapi\ApiControl\ApiController;
use Phr\Shell\Http\IHttpMethod;
use Phr\Griffin\Service\LoginService;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiControl\Response;
use Phr\Griffin\Contracts\Login\LoginResponse;
use Phr\Griffin\Contracts\User\UserLoginRequest;


final class LoginController extends ApiController
{
    readonly private LoginService $service;

    public function __construct()
    {
        $this->service = new LoginService;
    }
    /**
     * Route auth/login
     */
    public function controller(): void
    {   
        switch ( SHELL::route(1) ) 
        {
            default: $this->methodController(); break;
        }
    }
    /**
     * Route auth/login
     */
    public function methodController(): void
    {   
        switch ( self::method() ) 
        {   
            /**
             * Route GET auth/login/[realmId]
             * Get realm public rsa
             */
            case IHttpMethod::GET: Response::Ok($this->service->serviceProtocol()); break;

            case IHttpMethod::POST: Response::Ok($this->service->userLogin()); break;
        }
    }
}