<?php


namespace Phr\Griffin\Controllers;

use Phr\Webapi\ApiControl\ApiController;
use Phr\Shell\Http\IHttpMethod;
use Phr\Shell\Shell;
use Phr\Griffin\Service\AdminService;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiControl\Response;
use Phr\Griffin\GriffinBase\GriffinShell;
use Phr\Griffin\Contracts\Security\PublicGlobalResponse;
use Phr\Griffin\Contracts\User\MasterUserRequest;
use Phr\Griffin\Contracts\User\MasterLoginRequest;
use Phr\Griffin\Contracts\User\NewUserRequest;

use Phr\Webapi\ApiBase\ApiShell;
use Phr\Webapi\ApiControl\Contracts\ListResponse;
use Phr\Griffin\Session\MasterSessionControl;

final class AdminController extends ApiController
{   
    readonly private AdminService $service;

    readonly private MasterSessionControl $session;


    public function __construct()
    {
        $this->service = new AdminService;

        $this->session = new MasterSessionControl;

    }

    public function controller(): void
    {  
        switch ( Shell::route(2) ) 
        {   
            case 'users': $this->adminUsersMethod(); break;
            case 'master': $this->methodMasterController(); break;
            case 'login': $this->methodMasterLoginController(); break;
            default: $this->methodController(); break;
        }
    }
    public function methodController(): void
    {   
        $service = new AdminService;
        switch ( self::method() ) 
        {
            case IHttpMethod::GET: self::response(RC::OK,$service->contract()); break;

            case IHttpMethod::POST: $service->createNewRealm(); break; 

            
            default:
                # code...
                break;
        }
    }
    public function adminUsersMethod()
    {  
        $this->session->validate();
        
        switch ( self::method() ) 
        {
            case IHttpMethod::GET: Response::Ok(new ListResponse($this->service->getAllUsersInRealm(self::parameters()))); break;

            case IHttpMethod::POST: 
                $this->session->decryptPost();
                Response::Accepted($this->service->createNewUser(new NewUserRequest(ApiShell::decryptedIncomeing())));
            break;
        }
    }
    public function methodMasterController(): void
    {   
        switch ( self::method() ) 
        {   
            case IHttpMethod::GET: Response::Ok(new PublicGlobalResponse()); break;
            case IHttpMethod::POST: Response::Accepted($this->service->createNewMasterUser(new MasterUserRequest(ApiShell::decryptedIncomeing()))); break;
        }
    }
    public function methodMasterLoginController(): void
    {   
        switch ( self::method() ) 
        {
            case IHttpMethod::GET: Response::Ok($this->service->masterToken()); break;
            case IHttpMethod::POST: Response::Ok($this->service->masterLogin(new MasterLoginRequest(ApiShell::decryptedIncomeing()))); break;
        }
    }
}