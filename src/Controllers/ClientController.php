<?php


namespace Phr\Griffin\Controllers;

use Phr\Webapi\ApiControl\ApiController;
use Phr\Shell\Http\IHttpMethod;
use Phr\Shell\Shell;
use Phr\Griffin\Service\ClientService;
use Phr\Webapi\ApiControl\ResponseCode as RC;

final class ClientController extends ApiController
{
    public function controller(): void
    {   
        switch ( Shell::route(1) ) 
        {
            default: $this->methodController(); break;
        }
    }
    public function methodController(): void
    {   
        $service = new ClientService;
        switch ( self::method() ) 
        {
            case IHttpMethod::GET: echo "ok";; break;

            case IHttpMethod::POST: $service->createNewClient(); break;

            
            default:
                # code...
                break;
        }
    }
}