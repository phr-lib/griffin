<?php


namespace Phr\Griffin\Controllers;

use Phr\Webapi\ApiControl\ApiController;
use Phr\Webapi\ApiControl\Response;
use Phr\Shell\Shell;
use Phr\Griffin\Service\AccessService;
use Phr\Webapi\ApiControl\Contracts\ListResponse;
use Phr\Griffin\Contracts\Access\ClientAccessRequest;
use Phr\Griffin\Contracts\Access\UserRolRequest;


final class AccessController  extends ApiController
{   
    readonly private AccessService $service;

    public function __construct()
    {
        $this->service = new AccessService;
    }

    public function controller(): void
    {  
        switch ( Shell::route(2) ) 
        {   
            case 'servers': $this->methodServerController(); break;
            case 'rols': $this->methodUserRolsController(); break;
            default: $this->methodController(); break;
        }
    }

    public function methodController(): void
    {   
        switch ( self::method() ) 
        {
            case self::GET: Response::Ok(new ListResponse($this->service->getAccess(self::parameters()))); break;

            case self::POST: Response::Accepted($this->service->newAccess(new ClientAccessRequest(self::posted()))); break; 

            case self::DELETE: Response::Accepted($this->service->deleteUserAccess(self::parameters())); break;
            
        }
    }
    public function methodServerController(): void
    {
        switch ( self::method() ) 
        {
            case self::GET: Response::Ok(new ListResponse($this->service->getServers(self::parameters()))); break;

            
        }
    }
    public function methodUserRolsController(): void
    {
        switch ( self::method() ) 
        {
            case self::GET: Response::Ok(new ListResponse([])); break;
            case self::POST: Response::Accepted($this->service->addNewAccessRole(new UserRolRequest(self::posted()))); break;
            case self::DELETE: Response::Accepted($this->service->deleteAccessRole(self::parameters()));
            
        }
    }
}