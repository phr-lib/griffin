<?php


namespace Phr\Griffin\Controllers;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Griffin\GriffinException;
use Phr\Griffin\Session\SessionControl;
use Phr\Griffin\Session\ActiveSession;
use Phr\Griffin\Service\TokenService;
use Phr\Webapi\ApiControl\ApiController;
use Phr\Shell\Http\IHttpMethod;
use Phr\Griffin\Service\UserService;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiControl\Response;


final class TokenController extends ApiController
{   
    readonly private ActiveSession $session;

    readonly private UserService $service;

    readonly private TokenService $tokenService;

    public function __construct()
    {   
        
        if(self::method() !== self::GET) throw new GriffinException(ERR::E5010001);
        
        $this->session = new ActiveSession;
        
        $this->tokenService = new TokenService($this->session);
       
        #$this->service = new UserService;
    }
    /**
     * Route GET ***
     */
    public function controller(): void
    {   
        switch ( SHELL::route(2) ) 
        {         
            /**
             * Path auth/token/userinfo
             *  */     
            case 'userinfo': Response::Ok($this->tokenService->userInfo()); break;
            /**
             * Paht auth/token/server
             */
            case 'server': Response::Ok($this->tokenService->serverToken()); break;

        }
    }
    
    
}