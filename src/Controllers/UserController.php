<?php


namespace Phr\Griffin\Controllers;

use Phr\Webapi\ApiControl\ApiController;
use Phr\Shell\Http\IHttpMethod;
use Phr\Griffin\Service\UserService;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Webapi\ApiControl\Response;


final class UserController extends ApiController
{
    readonly private UserService $service;

    public function __construct()
    {
        $this->service = new UserService;
    }

    public function controller(): void
    {   
        switch ( SHELL::route(1) ) 
        {   
            case 'token': $controller = new TokenController;$controller->controller(); break;
            default: $this->methodController(); break;
        }
    }
    public function methodController(): void
    {   
        
        switch ( self::method() ) 
        {
            case IHttpMethod::GET: Response::Ok($this->service->getUserById(SHELL::parameters())); break;

            case IHttpMethod::POST: $service->createNewRealm(); break;

            
            default:
                # code...
                break;
        }
    }
    
}