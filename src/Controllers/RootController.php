<?php


namespace Phr\Griffin\Controllers;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Webapi\ApiControl\ApiController;
use Phr\Shell\Http\IHttpMethod;

use Phr\Griffin\Controllers as ROUTE;

use Phr\Griffin\Controllers\RealmController;
use Phr\Griffin\Controllers\AdminController;
use Phr\Griffin\Controllers\UserController;
use Phr\Griffin\Controllers\LoginController;
use Phr\Griffin\Controllers\AccessController;
use Phr\Griffin\Controllers\TokenController;


use Phr\Eojwt\EoJwt;


use Phr\Griffin\Setup\StartUpSetup;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as APIERR;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Griffin\GriffinException;
use Phr\Griffin\Service\TokenService;
use Phr\Webapi\ApiControl\Response;

use Phr\Griffin\Contracts\StatusResponse;

final class RootController extends ApiController
{   
    public function controller(): void
    {   
        try
        {
            switch ( SHELL::route(1) ) 
            {
                case 'setup': $this->setup();Response::Ok(); break;
                case 'realm':$realm = new RealmController;$realm->controller(); break;
                case 'client':$realm = new ClientController;$realm->controller(); break;
                case 'admin':$realm = new AdminController;$realm->controller(); break;
                case 'user':$realm = new UserController;$realm->controller(); break;

                case 'login':  SHELL::controller(ROUTE\LoginController::class); break;

                case 'access': $login =  new AccessController;$login->controller(); break;
                case 'token': SHELL::controller(ROUTE\TokenController::class); break;



                
                default: $this->methodController(); break;
            }
        }catch(GriffinException $error)
        {
            throw new WebApiException($error->responseCode(), APIERR::E5600000, $error->getMessage());
        }
    }
    private function methodController()
    {
        switch ( self::method() ) 
        {
            case IHttpMethod::GET:  Response::Ok(new StatusResponse());
                                    
                # code...
                break;
            
            default:
                # code...
                break;
        }
    }
    
    private function setup()
    {   
        $setup = new StartUpSetup;
        $setup->run();
    }
}