<?php

namespace Phr\Griffin;

use Phr\Webapi\Settings\AppSettings;

readonly class GriffinSettings 
{
    public AppSettings $apiSettings;

    public function __construct(
        AppSettings $_api_settings
    ){
        $this->apiSettings = $_api_settings;
    }
}