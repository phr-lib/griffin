<?php


namespace Phr\Griffin\Setup;

use Phr\Webapi\ApiBase\ApiShell as SHELL;
use Phr\Griffin\Griffin;
use Phr\Griffin\GriffinBase\Support\SupportFiles;
use Phr\Certificator\Crips;
use Phr\Certificator\SaveFile;
use Phr\Certificator\FileHandler\ConfigContent;
use Phr\Certificator\FileHandler\AplContent;
use Phr\Certificator\FileHandler\Line\ConfigLine;
use Phr\Certificator\FileHandler\Line\AlpLine;
use Phr\Certificator\FileHandler\FileSettings;
use Phr\Griffin\Service\RealmService;

class StartUpSetup
{
    public function run()
    {   
        $registrationId = Crips::generateUniqueKeyId("Idp_reg::");
        $registrationDate = new \DateTime('now');

        $solutionId = Griffin::settings()->solutionId;

        $configContent = new ConfigContent(new ConfigLine('REGISTRATION', $registrationId));

        $idpMigrations = new Migrations;
        $idpMigrations->startUpMigration();
        $realm = new RealmService;
        $realm->createNewRealm();
        try
        {   
            $config = new SaveFile(SHELL::projectFolder().SupportFiles::IDP_REGISTRATION_CONFIG->folder(), new FileSettings(
                SupportFiles::IDP_REGISTRATION_CONFIG->fileType(),
                Griffin::settings()->solutionId,
                Griffin::settings()->applicationId
            ));

            $config->create($configContent, SupportFiles::IDP_REGISTRATION_CONFIG->fileName());


        }catch(SaveFileError $error)
        {
            $code =  $error->getCode();
            $message =  $error->getMessage();
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, "[E:savefile:{$code}] application registration::{$message}[e]");
        }
        


    }
}