<?php


namespace Phr\Griffin\Setup;

use Phr\Webapi\Api;
use Phr\Webapi\ApiBase\ApiShell;
use Phr\Webapi\ApiTools\CoreService;
use Phr\Sqlbridge\SqlException;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiBase\Errors as ERR;

use Phr\Sqlbridge\SqlSettings;
use Phr\Sqlbridge\Migrations\Engine;
use Phr\Sqlbridge\Migrations\Type;
use Phr\Griffin\Entity\Realm;
use Phr\Griffin\Entity\Client;
use Phr\Griffin\Entity\Users;
use Phr\Griffin\Entity\Sessions;
use Phr\Griffin\Entity\ClientAccess;
use Phr\Griffin\Entity\UserRols;
use Phr\Certificator\SaveFile;
use Phr\Certificator\FileHandler\FileSettings;
use Phr\Certificator\SaveFileError;
use Phr\Certificator\FileHandler\Line\AplLine;
use Phr\Certificator\FileHandler\AplContent;

use Phr\Griffin\GriffinBase\Support\SupportFiles as SF;
use Phr\Griffin\Entity\Server;

class Migration_init extends Engine
{
    public function migrate(array $_dbTable)
    {   
        $array = [
            
            [
                new Realm(self::VARCHAR, self::VARCHAR, self::VARCHAR, self::VARCHAR), 
                [[100, false, Type::PRIMARY->sql()],[100, false, Type::UNIQUE->sql()],[5000, false],[10000, false]]
                
            ],
            [
                new Client(self::VARCHAR, self::VARCHAR, self::VARCHAR, self::VARCHAR), 
                [[100, false, Type::PRIMARY->sql()],[100, false, Type::UNIQUE->sql()],[5000, false]]
            ],
            [
                new Users(self::VARCHAR, self::VARCHAR, self::VARCHAR, self::VARCHAR, self::VARCHAR), 
                [[100, false],[],[],[],[100, false],[100, false],[100, false],[100, false],[100, false]]
            ],
            [
                new ClientAccess(self::VARCHAR, self::VARCHAR, self::VARCHAR, self::VARCHAR),
                [[250, false],[250, false],[250, false],[100, null]]
            ],
            [
                new UserRols(self::VARCHAR, self::VARCHAR, self::VARCHAR),
                [[250, false],[250, false],[250, false]]
            ],
            [
                new Server(self::VARCHAR, self::VARCHAR, self::VARCHAR, self::VARCHAR),
                [[250, false],[250, false],[5000, false],[50, false]]
            ]
        ];#@end parser ***

        $this->parser($array);
    }
}

class Migrations extends CoreService
{
    public function startUpMigration()
    {   
        try
        {
            $migrations = new Migration_init($this->getSqlSettings());
            $migrations->migrate([['database', 'realm'],['database', 'client'],['database', 'users'],['database', 'session']]);
        }
        catch(SqlException $error)
        {
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010, $error->getMessage());
        }
        try 
        {
            $setupAplFile = new SaveFile(ApiShell::syssFolder().SF::IDP_MIGRATION_APL->folder(), 
            new FileSettings(   SF::IDP_MIGRATION_APL->fileType()
                                ,ApiShell::settings()->solutionId
                                ,ApiShell::settings()->applicationId
                                ,null
                            )
            );
            $content = new AplContent("BASIC SETUP", new AplLine(['session', "created"], 'Basic application databases'));
            $content->add(new AplLine(['logs', "created"], 'Application log db'));
            $setupAplFile->create($content, SF::IDP_MIGRATION_APL->fileName());

        }catch(SaveFileError $error)
        {
            throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5100300, $error->getMessage());
        }
        
    }
}