<?php


namespace Phr\Griffin\Contracts\Realm;

use Phr\Griffin\Entity\Server;

class NewServerRequest extends Server
{   
    
    public function __construct(object $data)
    {
        $this->realmId = $data->realmId;
        $this->serverId = $data->serverId;
        $this->publicPem = $data->publicPem;
        $this->serverIp = $data->serverIp;
    }
    public function toEntity(): Server
    {
        return(new Server(
            $this->realmId,
            $this->serverId,
            $this->publicPem,
            $this->serverIp
        ));
    }
}