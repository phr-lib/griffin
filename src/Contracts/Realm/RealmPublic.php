<?php


namespace Phr\Griffin\Contracts\Realm;

readonly class RealmPublic 
{   
    public string $realmPublic;

    public function __construct(
        string $realmPublic
    ){
        $this->realmPublic = $realmPublic;
    }
}