<?php


namespace Phr\Griffin\Contracts\Realm;

readonly class ServerStatus 
{   
    public string $realmId; 
    public string $serverId; 


    public function __construct(array $data)
    {
        $this->realmId = $data['realmId'];
        $this->serverId = $data['serverId'];
    }
}