<?php


namespace Phr\Griffin\Contracts\Realm;

use Phr\Griffin\Entity\Realm;

readonly class RealmResponse
{   
    public string $realmId; 

    public function __construct(array $data)
    {   
        $this->realmId = $data['realmId'];
    }
}