<?php


namespace Phr\Griffin\Contracts;

use Phr\Webapi\ApiControl\Contracts\Maintaince\DiagnozeResponse;
use Phr\Webapi\ApiBase\ApiShell;
use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\GriffinBase\Support\SupportFiles as SF;

readonly class StatusResponse extends DiagnozeResponse
{   
    public string $master;

    public string $idpSetup;

    public function __construct()
    {
        parent::__construct();
        $this->master = $this->set(file_exists(ApiShell::syssFolder().SF::MASTER_APL->file()));
        $this->idpSetup = $this->set(file_exists(ApiShell::syssFolder().SF::IDP_MIGRATION_APL->file()));
    }
}