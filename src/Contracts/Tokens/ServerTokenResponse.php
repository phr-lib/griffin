<?php


namespace Phr\Griffin\Contracts\Tokens;

readonly class ServerTokenResponse 
{   
    public string $serverToken; 

    public string $activeSession; 

    public function __construct(
        string $serverToken,
        string $activeSession
    ){
        $this->serverToken = $serverToken;
        $this->activeSession = $activeSession;
    }
}