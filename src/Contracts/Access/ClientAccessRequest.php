<?php


namespace Phr\Griffin\Contracts\Access;

use Phr\Griffin\Entity\ClientAccess;

class ClientAccessRequest extends ClientAccess
{
    public function __construct(object $data)
    {
        $this->realmId = $data->realmId;
        $this->serverId = $data->serverId;
        $this->userId = $data->userId;
        if(isset($data->userRole))
            $this->userRole = $data->userRole;
        else $this->userRole = null;
    }
    public function entity(): ClientAccess
    {
        return (new ClientAccess(
            $this->realmId,
            $this->serverId,
            $this->userId,
            $this->userRole
        ));
    }
}