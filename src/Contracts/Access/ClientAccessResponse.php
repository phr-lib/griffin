<?php


namespace Phr\Griffin\Contracts\Access;

readonly class ClientAccessResponse 
{   
    public string $realmId;

    public string $serverId;

    public string $userId;

    public function __construct(array $data)
    {
        $this->realmId = $data['realmId'];
        $this->serverId = $data['serverId'];
        $this->userId = $data['userId'];
    }
}