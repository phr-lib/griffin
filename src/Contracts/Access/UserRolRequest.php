<?php


namespace Phr\Griffin\Contracts\Access;

use Phr\Griffin\Entity\UserRols;

class UserRolRequest extends UserRols
{
    public function __construct(object $data)
    {
        $this->realmId = $data->realmId;
        $this->serverId = $data->serverId;
        $this->role = $data->role;
       
    }
    public function entity(): UserRols
    {
        return (new UserRols(
            $this->realmId,
            $this->serverId,
            $this->role
        ));
    }
}