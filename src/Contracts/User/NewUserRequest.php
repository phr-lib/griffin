<?php


namespace Phr\Griffin\Contracts\User;

use Phr\Griffin\GriffinBase\GriffinShell;

readonly class NewUserRequest 
{   
    public string $username; 

    public string $password;

    public string $realmId;

    public function __construct(string $_decoded_data)
    {   
        $data = GriffinShell::parseSslString($_decoded_data);
        $this->username = $data['username'];
        $this->password = $data['password'];
        $this->realmId = $data['realmId'];
    }
}