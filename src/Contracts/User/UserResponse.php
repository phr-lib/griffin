<?php


namespace Phr\Griffin\Contracts\User;

readonly class UserResponse
{   
    public string $userId;

    public string $username;


    public function __construct(array $data)
    {   
        $this->userId = $data['uuid'];
        $this->username = $data['username'];

    }
}