<?php


namespace Phr\Griffin\Contracts;

use Phr\Griffin\Entity\Realm;

readonly class ServiceResponse
{   
    public string $realmPublic; 

    public function __construct(string $realmPublic)
    {
        $this->realmPublic = $realmPublic;
    }
}