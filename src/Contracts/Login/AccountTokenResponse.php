<?php


namespace Phr\Griffin\Contracts\Login;

readonly class AccountTokenResponse 
{   
    public string $accountToken;

    public function __construct(string $_account_token)
    {
        $this->accountToken = $_account_token;
    }
}