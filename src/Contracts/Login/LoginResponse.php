<?php


namespace Phr\Griffin\Contracts\Login;

readonly class LoginResponse 
{   
    public string $refreshToken;

    public function __construct(string $_refresh_token)
    {
        $this->refreshToken = $_refresh_token;
    }
}