<?php


namespace Phr\Griffin\Contracts;

use Phr\Griffin\Entity\Realm;

readonly class UserResponse
{   
    public string $userId; 

    public function __construct(string $userId)
    {
        $this->userId = $userId;
    }
}