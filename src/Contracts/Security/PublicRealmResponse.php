<?php


namespace Phr\Griffin\Contracts\Security;

use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Griffin\GriffinBase\GriffinShell;

readonly class PublicRealmResponse
{   
    public string $phex; 

    public function __construct()
    {
        $this->phex = GriffinShell::getRealmPublic();
    }
}