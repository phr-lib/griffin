<?php


namespace Phr\Griffin\Contracts\Security;

use Phr\Webapi\ApiTools\ApiSecure;

readonly class PublicGlobalResponse
{   
    public string $phex; 

    public function __construct()
    {
        $this->phex = ApiSecure::publicRsaHex();
    }
}