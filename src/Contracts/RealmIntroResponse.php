<?php


namespace Phr\Griffin\Contracts;

readonly class RealmIntroResponse
{   
    public string $introToken;

    public function __construct(
        string $introToken
    ){
        $this->introToken = $introToken;
    }
}