<?php


namespace Phr\Griffin;

use Phr\Griffin\GriffinBase\Errors;
use Phr\Webapi\ApiControl\ResponseCode;
use Phr\Webapi\WebApiException;
use Phr\Webapi\ApiBase\Errors as ERR;
use Phr\Webapi\ApiControl\ResponseCode as RC;


class GriffinException extends \Exception 
{   
    private ResponseCode $responseCode;


    public function __construct(Errors $_griffin_error, string $_messge = null)
    {   
        switch ($_griffin_error) 
        {
            case Errors::E5010000: $RC = RC::INTERNAL_SERVER_ERROR; $WAERR = ERR::E5100300; break;
            
            default: $RC = RC::INTERNAL_SERVER_ERROR; $WAERR = ERR::E5100300; break;
        }
        $grfErrorCode = $_griffin_error->code();
        $grfErrorMesage = $_griffin_error->message();

        $sideMessage = "GRFERR:[{$grfErrorCode}] Message: {$grfErrorMesage} [i] {$_messge}";
        throw new WebApiException($RC, $WAERR, $sideMessage);
    }
    
}