<?php


namespace Phr\Griffin\Entity;

use Phr\Sqlbridge\Entity;

class Sessions extends Entity
{   
    public string $sessionId;

    public string $clientId;

    public string $realmId;

    public string $userId;

    public string $fp1;


    public function __construct(
        string $sessionId
        ,string $clientId
        ,string $realmId
        ,string $userId
        ,string $fp1
    ){  
        $this->sessionId = $sessionId;
        $this->clientId = $clientId;
        $this->realmId = $realmId;
        $this->userId = $userId;
        $this->fp1 = $fp1;
    }
}