<?php


namespace Phr\Griffin\Entity;

use Phr\Sqlbridge\Entity;

class Realm extends Entity
{
    public string $realmId;

    public string $registrationId;

    public string $publicPem;

    public string $privatePem;

    public function __construct(
        string $realmId
        ,string $registrationId
        ,string $publicPem
        ,string $privatePem
    ){
        $this->realmId = $realmId;
        $this->registrationId = $registrationId;
        $this->publicPem = $publicPem;
        $this->privatePem = $privatePem;
    }
}