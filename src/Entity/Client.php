<?php


namespace Phr\Griffin\Entity;

use Phr\Sqlbridge\Entity;

class Client extends Entity
{
    public string $clientId;

    public string $realmId;

    public string $publicPem;

    public function __construct(
        string $realmId
        ,string $clientId
        ,string $publicPem
    ){  
        $this->clientId = $clientId;
        $this->realmId = $realmId;
        $this->publicPem = $publicPem;
    }
}