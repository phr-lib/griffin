<?php


namespace Phr\Griffin\Entity;

use Phr\Sqlbridge\Entity;

class UserRols extends Entity
{   
    public string $realmId;

    public string $clientId;

    public string $role;

    public function __construct(
        string $realmId
        ,string $clientId
        ,string $role
    ){  
        $this->realmId = $realmId;
        $this->clientId = $clientId;
        $this->role = $role;
    }
}