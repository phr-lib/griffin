<?php


namespace Phr\Griffin\Entity;

use Phr\Sqlbridge\Entity;
use Phr\Sqlbridge\FullEntity;


class Users extends FullEntity
{   
    public string $realmId;

    public string $username;

    public string $password;

    public string $publicPem;

    public string $privatePem;

    public function __construct(
        string $realmId
        ,string $username
        ,string $password
        ,string $publicPem
        ,string $privatePem
    ){  
        parent::__construct();
        $this->realmId = $realmId;
        $this->username = $username;
        $this->password = $password;
        $this->publicPem = $publicPem;
        $this->privatePem = $privatePem;
    }
}