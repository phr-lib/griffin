<?php


namespace Phr\Griffin\Entity;

use Phr\Sqlbridge\Entity;

class Server extends Entity
{
    public string $realmId;

    public string $serverId;

    public string $publicPem;

    public string $serverIp;


    public function __construct(
        string $realmId
        ,string $serverId
        ,string $publicPem
        ,string $serverIp
    ){  
        $this->serverId = $serverId;
        $this->realmId = $realmId;
        $this->publicPem = $publicPem;
        $this->serverIp = $serverIp;
    }
    public static function entity(array $data): self
    {   
        return new self(
            $data['realmId'],
            $data['serverId'],
            $data['publicPem'],
            $data['serverIp']
        );
    }
}