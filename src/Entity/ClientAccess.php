<?php


namespace Phr\Griffin\Entity;

use Phr\Sqlbridge\Entity;

class ClientAccess extends Entity
{   
    public string $realmId;

    public string $serverId;

    public string $userId;

    public string|null $userRole;

    public function __construct(
        string $realmId
        ,string $serverId
        ,string $userId
        ,string|null $userRole

    ){  
        $this->realmId = $realmId;
        $this->serverId = $serverId;
        $this->userId = $userId;
        $this->userRole = $userRole;
    }
}