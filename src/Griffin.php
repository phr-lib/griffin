<?php


namespace Phr\Griffin;

use Phr\Shell\Shell;
use Phr\Griffin\Api\Controller;
use Phr\Griffin\GriffinException;
use Phr\Griffin\Controllers\RootController;
use Phr\Griffin\Session\HeaderInterceptor;

final class Griffin extends GriffinBase\GriffinBase implements IGriffin
{   
    public function intercept()
    {   
        if(Shell::route() == 'auth')
        {   
            HeaderInterceptor::intercept();
            $route = new RootController;
            $route->controller();
            exit;
        }
        
    }
    
   
}