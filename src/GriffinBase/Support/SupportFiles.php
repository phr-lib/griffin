<?php


namespace Phr\Griffin\GriffinBase\Support;

use Phr\Webapi\ApiBase\Support\SupportFiles as ApiSupport;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Certificator\FileHandler\FileVars;


enum SupportFiles
{
    case IDP_REGISTRATION_CONFIG;

    case IDP_MASTER_CERT;

    case SESSION_FILE;

    case MASTER_APL;

    case MASTER_CERT;

    case IDP_MIGRATION_APL;

    public function folder(): string 
    {
        return match ($this) {
            self::IDP_REGISTRATION_CONFIG => ApiSupport::REGISTRATION_CONFIG_FILE->folder(),
            self::IDP_MASTER_CERT => ApiSupport::DB_CERT->folder(),
            self::SESSION_FILE => ApiSupport::SESSION_CERT->folder(),
            self::MASTER_APL => ApiSupport::REGISTRATION_FILE->folder(),
            self::MASTER_CERT => ApiSupport::DB_CERT->folder(),
            self::IDP_MIGRATION_APL => ApiSupport::REGISTRATION_FILE->folder(),

        };
    }
    public function fileName(): string 
    {
        return match ($this) {
            self::IDP_REGISTRATION_CONFIG => 'idp',
            self::IDP_MASTER_CERT => 'apm',
            self::MASTER_APL => 'master',
            self::MASTER_CERT => 'master',
            self::IDP_MIGRATION_APL => 'idpsetup'

        };
    }
    public function file(): string 
    {
        return match ($this) {
            self::IDP_REGISTRATION_CONFIG =>    self::IDP_REGISTRATION_CONFIG->folder()
                                                .self::IDP_REGISTRATION_CONFIG->filename()
                                                .GR::COMA.FileVars::FILE_SFCRY->ext(),
            
            self::IDP_MASTER_CERT =>            self::IDP_MASTER_CERT->folder()
                                                .self::IDP_MASTER_CERT->filename()
                                                .GR::COMA.FileVars::CERT_SFCRT_SIGNED->ext(),
            
            self::MASTER_APL =>            self::MASTER_APL->folder()
                                                .self::MASTER_APL->filename()
                                                .GR::COMA.FileVars::FILE_APL->ext(),

            self::MASTER_CERT =>            self::MASTER_CERT->folder()
                                                .self::MASTER_CERT->filename()
                                                .GR::COMA.FileVars::FILE_SFCRY->ext(),

            self::IDP_MIGRATION_APL =>      self::IDP_MIGRATION_APL->folder()
                                                .self::IDP_MIGRATION_APL->filename()
                                                .GR::COMA.FileVars::FILE_SFCRY->ext(),
        };
    }
    public function fileType(): string 
    {
        return match ($this) {
            self::IDP_REGISTRATION_CONFIG => ApiSupport::REGISTRATION_CONFIG_FILE->fileType(),
            self::IDP_MASTER_CERT => ApiSupport::DB_CERT->fileType(),
            self::MASTER_APL => ApiSupport::REGISTRATION_FILE->fileType(),
            self::MASTER_CERT => ApiSupport::DB_CERT->fileType(),
            self::IDP_MIGRATION_APL => ApiSupport::REGISTRATION_FILE->fileType(),
        };
    }
    public function dateFormat(): string 
    {
        return match ($this) {
            self::IDP_REGISTRATION_CONFIG => 'Y-m-d\\TH:i:sP',
            self::IDP_MASTER_CERT => 'Y-m-d\\TH:i:sP',
            self::MASTER_APL => 'Y-m-d\\TH:i:sP',
            self::MASTER_CERT => 'Y-m-d\\TH:i:sP',
            self::IDP_MIGRATION_APL => 'Y-m-d\\TH:i:sP',
        };
    }
    public function ext(): string 
    {
        return match ($this) {
            self::IDP_MASTER_CERT => 'cert',
            self::SESSION_FILE => 'cert',
            self::MASTER_APL => FileVars::SETUP_APL->ext(),
            self::MASTER_CERT => FileVars::DB_CERT->ext(),
            self::IDP_MIGRATION_APL => FileVars::SETUP_APL->ext()

        };
    }

}

