<?php

namespace Phr\Griffin\GriffinBase;

class Endpoints 
{   
    public string $issuer;

    public function __construct(
        string $_issuer
    ){
        $this->issuer = $_issuer;
    }
}