<?php


namespace Phr\Griffin\GriffinBase;

use Phr\Shell\Shell;
use Phr\Webapi\ApiBase\ApiShell;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Webapi\WebApiException;

use Phr\Griffin\GriffinBase\Support\SupportFiles as SF;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Griffin\Service\RealmService;

use Phr\Certificator\Encryption;
use Phr\Certificator\SaveFile;
use Phr\Certificator\SaveFileError;


class GriffinShell extends ApiShell
{   
    public const MASTER_REALM = 'masterRealm';

    public const MASTER_CLIENT = 'master';

    public static function controller(string $_controller_class): void
    {
        $initClass = new $_controller_class;
        $initClass->controller();
    }

    public static function masterId()
    {
        $keyBlocks = ApiSecure::retriveApplicationKey();
        return md5($keyBlocks[0]);
    }
    public static function masterEncryptor()
    {
        $keyBlocks = ApiSecure::retriveApplicationKey();
        return md5($keyBlocks[0]*$keyBlocks[1]*$keyBlocks[2]);
    }
    public static function decodeRealmPost()
    {   
        $decryptedKey = Encryption::sslPrivateDecrypt(self::getRealmPrivate(), self::authorization());
        $decrypt = new Encryption($decryptedKey);
        $payload = self::incomeing();
        $payload = Encryption::decodeHex($payload);
        $result = $decrypt->fernetDecrypt($payload);
        return self::parseSslString($result);
        
    }
    public static function getRealmPublic()
    {
        $realmService = new RealmService;
        return Encryption::encodeHex($realmService->realmPublic());
    }
    public static function getRealmPrivate(string $realmId)
    {
        $realmService = new RealmService;
        return $realmService->realmPrivate($realmId);
    }
    public static function parseSslString(string $_content): array
    {   
        $result = [];
        $content = preg_replace('/\(\'/', '', $_content);
        $content = preg_replace('/\',\)/', '', $content);

        $brake = explode('&', $content);
        foreach($brake as $vars)
        {
            $varPars = explode('=',$vars);
            $result[$varPars[0]] = $varPars[1];
        }
        return $result;
        
    }
    public static function realmEncryptedResponse(object $_content)
    {
        $plaintext = "message to be encrypted";
        
        
        
        $iv = "2354658123546521";

        $enryptedKey = self::authorization();
        $key = Encryption::sslPrivateDecrypt(self::getRealmPrivate(), $enryptedKey);

        #$ciphertext = openssl_encrypt($plaintext, $cipher, $key, $options=0, $iv);
        #return self::sslEncryptOptions(json_encode($_content), $key, $iv);
        $response =  Encryption::sslEncryptOptions(json_encode($_content), $key, $iv);
        Shell::secureResponse(Encryption::encodeHex($response));
    }
    public static function retriveGriffinFile(SF $SF, string|null $_key = null)
    {   
        $file = self::projectFolder().$SF->file();
        
        try
        {   
            if(!file_exists($file))
                    throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010);
                
            return SaveFile::read($file, $_key);
                        
        }catch(SaveFileError $error)
        {   
            $code = $error->getCode();
            $message = $error->getMessage();
            #throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5655000,"[E:loadfile:{$code}] ::{$message}[e]");
        }
    }
    public static function retriveSessionFile(string $_user_id, string|null $_key = null)
    {   
        $file = self::projectFolder().SF::SESSION_FILE->folder().$_user_id.'.'.SF::SESSION_FILE->ext();
        
        try
        {   
            if(!file_exists($file))
                    throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5604010);
                
            return SaveFile::read($file, $_key);
                        
        }catch(SaveFileError $error)
        {   
            $code = $error->getCode();
            $message = $error->getMessage();
            #throw new WebApiException(RC::INTERNAL_SERVER_ERROR, ERR::E5655000,"[E:loadfile:{$code}] ::{$message}[e]");
        }
    }
}