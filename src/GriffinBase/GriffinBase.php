<?php


namespace Phr\Griffin\GriffinBase;


use Phr\Griffin\GriffinSettings;
use Phr\Webapi\Settings\AppSettings;
use Phr\Webapi\Api;
use Phr\Webapi\ApiBase\ApiShell as SHELL;

abstract class GriffinBase 
{   
    protected static AppSettings $apiSettings;

    public function __construct()
    {   
        self::$apiSettings = SHELL::settings();
    }

    public static function rootDir(): string
    {
        return SHELL::projectFolder();
    }
    public static function settings(): AppSettings
    {
        return self::$apiSettings;
    }
}