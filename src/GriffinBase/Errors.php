<?php


namespace Phr\Griffin\GriffinBase;


enum Errors 
{
    case E5655000;
    /// Expcectations
    case E5010000;
    case E5010001;

    /// SessionControl
    case E5030000;
    case E5030001;
    case E5030005;



    /// Jwt errors
    case E5030100;
    case E5030101;



    public function code(): int 
    {
        return match ($this) 
        {
            self::E5655000 => 5655000,

            self::E5010000 => 5010000,
            self::E5010001 => 5010001,


            self::E5030000 => 5030000,
            self::E5030001 => 5030001,
            self::E5030005 => 5030005,



            self::E5030100 => 5030100,
            self::E5030101 => 5030101,



        
        };
    }

    public function message(): string 
    {
        return match ($this) 
        {
            self::E5655000 => 'griffin error',

            self::E5010000 => 'expectations',
            self::E5010001 => 'method not allowed',


            self::E5030000 => 'session',
            self::E5030001 => 'session structure',
            self::E5030005 => 'session keys',



            self::E5030100 => 'no jwt',
            self::E5030101 => 'jwt message',





        };
    }
}