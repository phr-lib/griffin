<?php

namespace Phr\Griffin\Tokens;

use Phr\Griffin\Tokens\Generator\JwtGenerator;
use Phr\Eojwt\Tokens\RefreshJwt as Payload;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Eojwt\Accounts\ActiveSession;



class RefreshJwt extends JwtGenerator 
{   
    private Payload $payload;

    public function generate(ActiveSession $_session_data): void
    {   
        $this->generateHeader();
        $this->payload = new Payload;
        $this->payload->setData([
            self::$settings->issuer
            ,self::$settings->realmId
            ,self::$settings->clientId
        ]);
        $this->payload->add($_session_data);
       
    }
    public function sign(string|null $_signing_key = null): void
    {    
        parent::setSignatureKey($_signing_key);
        $this->generateSignature();
    }
    public function token(): string
    {   
        return(
            $this->header->encode().GR::COMA.
            $this->payload->encode().GR::COMA.
            $this->signature->encode()
        );
    }
}