<?php

namespace Phr\Griffin\Tokens;

use Phr\Eojwt\EoJwtBase\TokenType;

/**
 * 
 * Generator settings! 
 * 
 * 
 */
readonly class Settings
{   
    public TokenType $tokenType;

    public string $issuer;

    public string $realmId;

    public string $clientId;

    public string|null $key;

    public string|null $publicRsa;

    public string|null $privateRsa;

    public int|null $ttlMinutes;

    public int|null $ttlHours;

    public int|null $ttlDays;


    public function __construct(
        TokenType $_token_type
        ,string $_issuer
        ,string $_realm_id
        ,string $_client_id
        ,string|null $_key = null
        ,string|null $_public_rsa = null
        ,string|null $_private_rsa = null
        ,int|null $_ttl_minutes = 1
        ,int|null $_ttl_hours = null
        ,int|null $_ttl_days = null
    ){
        $this->tokenType = $_token_type;
        $this->issuer = $_issuer;
        $this->realmId = $_realm_id;
        $this->clientId = $_client_id;
        $this->key = $_key;
        $this->publicRsa = $_public_rsa;
        $this->privateRsa = $_private_rsa;
        $this->ttlMinutes = $_ttl_minutes;
        $this->ttlHours = $_ttl_hours;
        $this->ttlDays = $_ttl_days;

    }
}