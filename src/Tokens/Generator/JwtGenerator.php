<?php

namespace Phr\Griffin\Tokens\Generator;

use Phr\Griffin\Tokens\Generator\TokenGeneratorBase\JwtGeneratorBase;
use Phr\Griffin\Tokens\Settings;


use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Eojwt\JwtModel\Signatures\JwtSignature;
use Phr\Eojwt\JwtModel\Signatures\SubSig\RefreshSubSignature;

use Phr\Eojwt\JwtModel\Signatures\EoJwtSignature;
use Phr\Eojwt\JwtModel\Signatures\EoSecureSignature;
use Phr\Eojwt\JwtModel\Signatures\EncrySignature;
use Phr\Eojwt\JwtModel\Signatures\EoJwtSignatureVertification;
use Phr\Eojwt\JwtModel\Signatures\SimpleTimeSignature;

use Phr\Griffin\Tokens\Tools\Encry;

/**
 * @abstract
 * 
 * 
 * 
 * Set creation date, set
 * expire base date.
 * 
 * 
 */
abstract class JwtGenerator extends JwtGeneratorBase
{   

    /// CONSTRUCT ***
    public function __construct(Settings $_settings)
    {   
        parent::__construct($_settings);
        
        $this->created = new \DateTime('now');
        $this->expire = clone $this->created;
        $this->issuer = $_settings->issuer;      
    }
    /**
     * 
     * @access protected
     * @method generates headers. All Tokens
     * have same form and content.
     * 
     */
    protected function generateHeader(): void
    {           
        $this->header->issued = $this->created;
        $this->header->type(parent::$tokenType);
        $this->expire();
        $this->header->tokenId = $this->formTokenId();
    }
    protected function generateSignature(string|null $_content_hash = null): void
    {   
        switch ( self::$tokenType ) 
        {
            case TokenType::PHRJWT: $this->generateJwtSignature(); break;
            case TokenType::PPPJWT: $this->generateJwtSignature(); break;
            case TokenType::ACCJWT: $this->generateSecureJwtSignature(); break;
            case TokenType::REFJWT: $this->generateEncrySignature(); break;
            case TokenType::SERJWT: $this->generateSecureJwtSignature(); break;
            case TokenType::SECJWT: $this->generateJwtSignature(); break;
            case TokenType::ACTJWT: $this->generateSimpleTimeSignature(); break;

        }
    }
    /**
     * @access private
     */
    private function expire()
    {   
        $ttl = self::$settings->ttlMinutes;
        $longlifetime = self::$settings->ttlDays;
        $ttlhours = self::$settings->ttlHours;
        if($longlifetime == null)
        {   
            if($ttlhours == null)
            {
                $this->expire->add(new \DateInterval("PT".$ttl."M"));

            }else $this->expire->add(new \DateInterval("PT".$ttlhours."H"));
            
        }else $this->expire->modify('+'.$longlifetime.' day');
        
        $this->header->expire = $this->expire->format('Y-m-d\\TH:i:sP');
    }
    private function formTokenId()
    {   
        self::$expiretime = $this->expire->getTimestamp();
        return self::$tokenId.GR::COMA.self::$expiretime;
    }
    private function generateSimpleTimeSignature()
    {   
        $this->signature = new SimpleTimeSignature;
        $this->signature->signature = Encry::simpleTimeSig($this->timeHash(), self::$signatureKey);
    }
    /**
     * User for PHRJWT. Default normal 
     * jwt signature.
     */    
    private function generateJwtSignature(): void
    {   
        $this->signature = new JwtSignature;
        $this->signature->signature = $this->gsjs(); 
    }
    /**
     * @method creates EoSecure jwt signature.
     */
    private function generateEncrySignature(): void
    {   
        $signature = new EncrySignature;
        $signature->params(parent::$tokenType);
        /**
         * Create new sub signature.
         */
        $subsignature = new RefreshSubSignature(self::$settings->key, $this->timeHash());
        /**
         * Encrypt sub signature.
         */
        $encrypt = Encry::encryptContentWithNewKey($subsignature);
        /**
         * Load encrypted subsignature.
         */
        $signature->encrySignature = Encry::hexIt($encrypt[0]);
        /**
         * Encrypt subsignature enryptor with 
         * public key. 
         */
        $signature->finalSignature(Encry::hexIt(Encry::signWithpublic(parent::$signatureKey, $encrypt[1])));

        $this->signature = $signature;      
    }
    private function generateSecureJwtSignature(): void
    {   
        $signature = new EoSecureSignature;
        $signature->params(parent::$tokenType);
        $signature->finalSignature(self::gsjs());
        $this->signature = $signature;
    }
    private static function gsjs(): string 
    {   
        return match ( self::$tokenType ) 
        {   
            TokenType::PHRJWT => Encry::signWithPrivate(parent::$signatureKey, self::$contentEncryptor),
            TokenType::PPPJWT => Encry::signWithpublic(parent::$signatureKey, self::$contentEncryptor),
            TokenType::ACCJWT => Encry::signWithPrivate(parent::$signatureKey, self::$contentEncryptor),
            TokenType::SERJWT => Encry::signWithPublic(parent::$signatureKey, self::$contentEncryptor),
            TokenType::SECJWT => Encry::signWithPrivate(parent::$signatureKey, self::$contentEncryptor),
        };
    }
}