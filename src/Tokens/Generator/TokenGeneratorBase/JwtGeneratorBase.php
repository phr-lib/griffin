<?php

namespace Phr\Griffin\Tokens\Generator\TokenGeneratorBase;

use Phr\Eojwt\EoJwtBase\EoJwtBase;
use Phr\Griffin\Tokens\Settings;

use Phr\Certificator\Crips;
use Phr\Eojwt\EoJwtBase\TokenType;

/**
 * 
 * 
 * @abstract 
 * 
 * 
 * Parse settings, create new 
 * token type.
 * 
 * 
 * 
 */
abstract class JwtGeneratorBase extends EoJwtBase
{   
    abstract public function sign(string|null $_signing_key = null): void;
    abstract public function token(): string;
    /**
     * @access protected
     * @var static
     * Settings: token type, issuer, key, 
     * rsa keys.
     * @var Settings
     */
    protected static Settings $settings;

    public function __construct(Settings $_settings)
    {   
        # Construct JwtBase
        parent::__construct();

        self::$settings = $_settings;
        
        self::$tokenType = $_settings->tokenType;

        self::$tokenId = Crips::generateUniqueKeyId("", false);
    }
    protected static function setSignatureKey(string $_signing_key = null)
    {   
        if($_signing_key == null)
        {
            switch ( self::$tokenType ) 
            {
                case TokenType::PHRJWT: parent::setRsaContent(self::$settings->privateRsa); break;
                case TokenType::PPPJWT: parent::setRsaContent(self::$settings->publicRsa); break;
                case TokenType::ACCJWT: parent::setRsaContent(self::$settings->privateRsa); break;
                case TokenType::REFJWT: parent::setRsaContent(self::$settings->publicRsa); break;
                case TokenType::SERJWT: parent::setRsaContent(self::$settings->publicRsa); break;
                case TokenType::SECJWT: parent::setRsaContent(self::$settings->privateRsa); break;
            }
        }else parent::$signatureKey = $_signing_key;
    }
    

}