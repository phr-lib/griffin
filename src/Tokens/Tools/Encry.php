<?php

namespace Phr\Griffin\Tokens\Tools;

use Phr\Certificator\Encryption;
use Phr\Eojwt\EoJwtTools\EoJwtEncoder;
use Phr\Eojwt\EoJwtTools\Verification;

class Encry extends EoJwtEncoder
{
    public static function signWithPrivate( string $_key, string $_content ): string
    {   
        Verification::privateRsa($_key);
        return Encryption::sslPrivateEncrypt($_key, $_content);
    }
    public static function signWithpublic( string $_key, string $_content ): string
    {   
        Verification::publicRsa($_key);
        return Encryption::sslPublicEncrypt($_key, $_content);
    }
    public static function hexIt(string $_content): string
    {
        return bin2hex($_content);
    }
    public static function encryptContentWithNewKey( object $_content ): array
    {   
        $key = Encryption::generate32bytsKey();
        $encrypt = new Encryption($key);
        return([
            $encrypt->fernetEncrypt($_content->json()),
            $key
        ]);
    }
    public static function encryptContent( string $_key, object $_content, string $_iv ): string
    {   
        return Encryption::encryptAES($_content->json(), $_key, $_iv);
    }
    public static function serverEncryptWithNewKey( object $_content ): array
    {
        $key = Encryption::generate32bytsKey();
        $encrypt = new Encryption($key);
        return([
            $encrypt->sslEncrypt($_content->json()),
            $key
        ]);
    }
    public static function simpleTimeSig(string $_date, string $_signature_key)
    {
        return Encryption::hashIt($_date.$_signature_key);
    }
}