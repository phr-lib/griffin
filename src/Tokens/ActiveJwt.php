<?php

namespace Phr\Griffin\Tokens;

use Phr\Griffin\Tokens\Generator\JwtGenerator;
use Phr\Eojwt\Tokens\ActiveJwt as Payload;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Eojwt\Accounts\ServerAccount;



class ActiveJwt extends JwtGenerator 
{   
    private Payload $payload;

    public function generate(ServerAccount $_server_account): void
    {   
        $this->generateHeader();
        $this->payload = new Payload;
        $this->payload->create($_server_account);    
    }
    public function sign(string|null $_signing_key = null): void
    {   
        parent::setSignatureKey(($_signing_key) ? $_signing_key: self::$settings->key);
        $this->generateSignature();
    }
    public function token(): string
    {   
        return(
            $this->header->encode().GR::COMA.
            $this->payload->encode().GR::COMA.
            $this->signature->signature
        );
    }
}