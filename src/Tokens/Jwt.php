<?php

namespace Phr\Griffin\Tokens;

use Phr\Griffin\Tokens\Generator\JwtGenerator;

use Phr\Eojwt\Tokens\Jwt as Payload;


use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;



class Jwt extends JwtGenerator 
{   
    private Payload $payload;

    public function generate(object $_content): void
    {   
        $this->generateHeader();
        $this->payload = new Payload;
        $this->payload->populate([
            self::$settings->issuer
            ,self::$settings->realmId
            ,self::$settings->clientId
        ], $_content);
        $this->payload->setTimeHash(parent::timeHash());
        
    }
    public function sign(string|null $_signing_key = null): void
    {   
        parent::setSignatureKey($_signing_key);
        self::$contentEncryptor = $this->payload->hash(self::$tokenType->algoContent());
        $this->generateSignature();
    }
    public function token(): string
    {   
        return(
            $this->header->encode().GR::COMA.
            $this->payload->encode().GR::COMA.
            $this->signature->signature
        );
    }
}