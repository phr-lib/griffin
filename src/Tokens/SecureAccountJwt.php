<?php

namespace Phr\Griffin\Tokens;

use Phr\Griffin\Tokens\Generator\JwtGenerator;
use Phr\Eojwt\Tokens\SecureAccountJwt as Payload;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Eojwt\Accounts\UserAccount;
use Phr\Griffin\Tokens\Tools\Encry;


class SecureAccountJwt extends JwtGenerator 
{   
    private Payload $payload;

    private string $encodedPayload;

    public function generate(UserAccount $_session_account): void
    {   
        $this->generateHeader();
        $this->payload = new Payload;
        $this->payload->add($_session_account);
        self::$contentEncryptor = md5($this->payload->json());
        $encodePayload = Encry::encryptContent(self::$settings->key, $this->payload, self::$sessionIv);
        $this->payload->setEcodedPayload($encodePayload);
    }
    public function sign(string|null $_signing_key = null): void
    {   
        parent::setSignatureKey($_signing_key);
        $this->generateSignature();
    }
    public function token(): string
    {   
        return(
            $this->header->encode().GR::COMA.
            $this->payload->encrypted().GR::COMA.
            $this->signature->signature
        );
    }

    
}