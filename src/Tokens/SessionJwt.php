<?php

namespace Phr\Griffin\Tokens;

use Phr\Griffin\Tokens\Generator\JwtGenerator;
use Phr\Eojwt\Tokens\SessionJwt as Payload;
use Phr\Certificator\CertificatorBase\Macro\Gradients as GR;
use Phr\Eojwt\Accounts\SessionAccount;


/**
 * 
 * Server jwt. Contains account id and
 * db cridentials keys.
 * 
 */
class SessionJwt extends JwtGenerator 
{   
    private Payload $payload;

    private string $encodedPayload;

    public function generate(SessionAccount $_session_account): void
    {   
        $this->generateHeader();
        $this->payload = new Payload;
        $this->payload->add($_session_account);
        $this->payload->timehash = $this->timeHash();
        # Generate new certificate key
        $encryption = Encry::encryptContentWithNewKey($this->payload);
        self::$contentEncryptor = $encryption[1];
        $this->payload->setEcodedPayload($encryption[0]);
    }
    public function sign(string|null $_signing_key = null): void
    {   
        parent::setSignatureKey($_signing_key);
        $this->generateSignature();
    }
    public function token(): string
    {   
        return(
            $this->header->encode().GR::COMA.
            $this->payload->encrypted().GR::COMA.
            $this->signature->signature
        );
    }

    
}