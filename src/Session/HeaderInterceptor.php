<?php


namespace Phr\Griffin\Session;

use Phr\Webapi\ApiControl\Authorization\HeaderAuthorization;
use Phr\Griffin\Session\SessionControl;

class HeaderInterceptor extends HeaderAuthorization
{
    public static function intercept()
    {    
        parent::controller();
        self::griffinHeadController();
    }
    private static function griffinHeadController()
    {   
        if(self::getHead() == '$$$') SessionControl::setSessionKey(self::getKey());
        #if(self::getHead() == '$&$') SessionControl::digestRealmLoginKey();
    }
}