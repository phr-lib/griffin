<?php


namespace Phr\Griffin\Session;

use Phr\Griffin\GriffinException;
use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Griffin\Service\SessionService;
use Phr\Eojwt\Accounts\ActiveSession as ActiveMetaSession;
use Phr\Eojwt\EoJwt;
use Phr\Eojwt\EoJwtException;
use Phr\Eojwt\Accounts\SessionFp;
use Phr\Certificator\Encryption;
use Phr\Griffin\Tokens\SecureAccountJwt;
use Phr\Griffin\Contracts\Login\AccountTokenResponse;
use Phr\Griffin\Entity\Server;
use Phr\Griffin\Tokens\ServJwt;
use Phr\Griffin\Tokens\ActiveJwt;

use Phr\Eojwt\Accounts\ServerAccount;
use Phr\Griffin\Contracts\Tokens\ServerTokenResponse;
use Phr\Eojwt\EoJwtBase\TokenType;


class ActiveSession extends ActiveMetaSession
{   
    readonly private SessionControl $session;

    readonly private SessionService $sessionService;

    readonly private string $sessionToken;

    public function __construct()
    {   
        $refreshToken = SHELL::authorization(); 
        
        if(!$refreshToken) throw new GriffinException(ERR::E5030100, 'missing bearer');
        
        try{
            
            $jwt = new EoJwt($refreshToken);
            
            $payload = $jwt->payload();
            
            $this->accountId = $payload->session->accountId;
            $this->session = new SessionControl(
                $payload->session->accountId,
                $payload->realmId,
                $payload->clientId
            );
            
            $jwt->validate($this->session->getPrivate(), false);
            
            $this->session->validate($payload->session);
           
            $this->sessionFp = $payload->session->sessionFp;

            #$this->sessionToken = $jwt->token();

            #$this->sessionService = new SessionService;

            #$this->validateActiveSession($payload->session->sessionFp);

        }catch(EoJwtException $error){
            throw new GriffinException(ERR::E5030101, $error->getMessage());
        } 
        
    }
    /**
     * @method creates user info token, 
     * based on active session.
     * @return AccountTokenResponse
     */
    public function userInfoToken(): AccountTokenResponse
    {
        $userInfoJwt = $this->session->createSecureJwt(SecureAccountJwt::class);
        $userInfoJwt->generate($this->session->userAccout());
        $userInfoJwt->sign($this->session->getPrivate());
        return new AccountTokenResponse($userInfoJwt->token());
    }
    public function serverToken(Server $server)
    {   
        $dehexPem = Encryption::decodeHex($server->publicPem);
        $userServerJwt = $this->session->createServerJwt(ServJwt::class, TokenType::SERJWT);
        $serverAccount = new ServerAccount(
            $server->serverId,
            $this->session->getSessionAccount(),
            '123.123.123.123'
        );
        $userServerJwt->generate($serverAccount);
        $userServerJwt->sign($dehexPem, false);
        $token = $userServerJwt->token();
        #$this->clientId = $this->session->getSessionAccount()->sessionId;
        $sessionServerJwt = $this->session->createServerJwt(ActiveJwt::class, TokenType::ACTJWT, $this->session->getSessionAccount()->enryptor);
        $sessionServerJwt->setSessionId($this->session->getSessionAccount()->sessionId);
        $sessionServerJwt->generate($serverAccount);
        $sessionServerJwt->sign();
        $activeSessionToken = $sessionServerJwt->token();

        return new ServerTokenResponse($token, $activeSessionToken);
    }
   
}