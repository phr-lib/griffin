<?php


namespace Phr\Griffin\Session;

use Phr\Griffin\GriffinException;
use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Griffin\Service\SessionService;
use Phr\Griffin\Session\SessionBase\SessionModel;
use Phr\Griffin\Entity\Users;
use Phr\Griffin\Tokens\SessionJwt;
use Phr\Griffin\Tokens\Settings;
use Phr\Eojwt\Accounts\SessionAccount;
use Phr\Eojwt\Accounts\UserAccount;
use Phr\Eojwt\Accounts\SessionFp;
use Phr\Eojwt\Accounts\ActiveSession;

use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Griffin\Tokens\RefreshJwt;
use Phr\Eojwt\EoJwt;
use Phr\Eojwt\Accounts\UserRols;
use Phr\Griffin\Contracts\Login\LoginResponse;
use Phr\Griffin\Contracts\Login\AccountTokenResponse;
use Phr\Webapi\ApiControl\Authorization\Sessions\SessionControl as ApiSession;
use Phr\Griffin\Session\SessionBase\UserSession;
use Phr\Griffin\Tokens\SecureAccountJwt;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Certificator\Encryption;


/**
 * 
 * Session control
 * 
 * @see SessionModel
 * 
 */
class SessionControl extends SessionModel
{   
    readonly private SessionService $service;

    # /// CONSTRUCT ***
    public function __construct(string $_user_id, string $_realm_id, string $_client_id)
    {
        parent::__construct($_user_id, $_realm_id, $_client_id);
        $this->service = new SessionService;
    }
    /**
     * 
     * @method validates active session.
     * Reads data from refresh token, 
     * validates session keys and fill up missing 
     * user data.
     * @param ActiveSession
     * @return true on validation
     * 
     */
    public function validate(ActiveSession $session): true
    {   
        /**
         * Read data from service
         */
        if(!isset($session->accountId)) throw new GriffinException(ERR::E5030001);
        $activeSession = $this->service->getSessionByUserId($session->accountId);
        /**
         * Handle session expire
         */
        $expire = (int)$activeSession[0]['expire'];
        /**
         * Match session keys
         */
        $keys = json_decode(Encryption::baseDecode($activeSession[0]['token']));
        if($session->sessionFp->fp1 !== $keys->fp1) throw new GriffinException(ERR::E5030005);
        if($session->sessionFp->fp2 !== $keys->fp2) throw new GriffinException(ERR::E5030005);
        if($session->sessionFp->fp3 !== $keys->fp3) throw new GriffinException(ERR::E5030005);
        if($session->sessionFp->fp4 !== $keys->fp4) throw new GriffinException(ERR::E5030005);
        if($session->sessionFp->fp5 !== $keys->fp5) throw new GriffinException(ERR::E5030005);

        $this->sessionId = $activeSession[0]['sessionId'];
        $this->username = $activeSession[0]['username'];
        /**
         * Create session account based on 
         * active session - refresh token.
         */
        $this->session = new SessionAccount(
            $session->accountId,
            $this->sessionId,
            $activeSession[0]['sessionTs'],
            $expire,
            $activeSession[0]['enryptor'],
            $activeSession[0]['sessionIv'],
            new SessionFp([
                $keys->fp1,
                $keys->fp2,
                $keys->fp3,
                $keys->fp4,
                $keys->fp5
            ])
        );
        return true;
    }
    public function getSessionAccount(): SessionAccount
    {
        return $this->session;
    }
    /**
     * @method create jwt secure header header.
     */
    public function createSecureJwt(string $jwtClass): object
    {   
        return new $jwtClass(new Settings(
            TokenType::SECJWT
            ,$this->issuer
            ,$this->realmId
            ,$this->clientId 
            ,$this->decryptProcessKey()
            ,null
            ,null
            ,10
        )); 
    }
    public function createServerJwt(string $jwtClass, TokenType $tokenType, string|null $key = null): object
    {   
        return new $jwtClass(new Settings(
            $tokenType
            ,$this->issuer
            ,$this->realmId
            ,$this->clientId 
            ,$key
            ,null
            ,null
            ,10
        )); 
    }

    public function getUserId(): string { return $this->userId; }
    
    public static function digestRealmEnryption(string $_private_realm)
    {   
        $decryptedKey = Encryption::sslPrivateDecrypt($_private_realm, SHELL::authorization());
        if(!$decryptedKey) throw new GriffinException(ERR::E5030000, 'private realm decryption');
        
        $payload = SHELL::incomeing();
        if(!$payload) throw new GriffinException(ERR::E5010000, 'no payload');
        $payload = Encryption::decodeHex($payload);

        $decrypt = new Encryption($decryptedKey);
        $result = $decrypt->fernetDecrypt($payload);
        if(!$result) throw new GriffinException(ERR::E5010000, 'payload decryption');
        return $result;
    }

    public function createNewUserSession(string $username, string $publicPem): LoginResponse
    {   
        $key = md5(rand());
        $userSession = new UserSession($this->userId, $username, new UserRols([]));

        $this->service->createNewUserSession($userSession);
        
        $jwtGenerator = new RefreshJwt(new Settings(
            TokenType::REFJWT
            ,$this->issuer
            ,$this->realmId
            ,$this->clientId 
            ,$key
            ,null
            ,null
            ,10
        ));

        $jwtGenerator->generate(new ActiveSession($this->userId, $userSession->sessionFp()));
        $jwtGenerator->sign($publicPem);
        $token = $jwtGenerator->token();
        
        return new LoginResponse($token);
    }
    ##############################################################
    ##############################################################
    ##############################################################

    public function createNewMasterSession(string $_username): LoginResponse
    {   
        $key = md5(rand());
        $userSession = new UserSession($this->userId, $_username, new UserRols(['admin']));
        $userSession->sessionAccout();
        $apiSession = new ApiSession($userSession->sessionAccout());
        $apiSession->createNewCertSession($key);
        
        $jwtGenerator = new RefreshJwt(new Settings(
            TokenType::REFJWT
            ,$this->issuer
            ,$this->realmId
            ,$this->clientId 
            ,$key
            ,null
            ,null
            ,10
        ));
        
        $jwtGenerator->generate(new ActiveSession($this->userId, $userSession->sessionFp()));
        $jwtGenerator->sign($this->publicRsa);
        $token = $jwtGenerator->token();
        
        return new LoginResponse($token);
    }
    public function validateMasterRefresh(object $_session, string $_decryptor)
    {   
        $certContent = SHELL::retriveSessionFile(SHELL::masterId(), $_decryptor);
       
        if($certContent->content->fingerprints->fp1 !== $_session->sessionFp->fp1) exit("session failed");
        $this->session = new SessionAccount(
            $certContent->content->userId,
            $certContent->content->sessionId,
            $certContent->content->sessionTs,
            $certContent->content->expire,
            $certContent->content->encryptor,
            $certContent->content->sessionIv,
            new SessionFp(
                [
                    $certContent->content->fingerprints->fp1,
                    $certContent->content->fingerprints->fp2,
                    $certContent->content->fingerprints->fp3,
                    $certContent->content->fingerprints->fp4,
                    $certContent->content->fingerprints->fp5,
                ]
            )
        );
        
        $this->sessionId = $certContent->content->sessionId;
       
        return true;

    }
    public function createMasterToken(): AccountTokenResponse
    {   

        $jwtGenerator = new SecureAccountJwt(new Settings(
            TokenType::SECJWT
            ,$this->issuer
            ,$this->realmId
            ,$this->clientId 
            ,self::$sessionKey
            ,null
            ,null
            ,10
        ));

        $jwtGenerator->generate($this->userAccout("ADMIN"));
        $jwtGenerator->sign($this->privateRsa);
        $token = $jwtGenerator->token();

        return new AccountTokenResponse($token);
    }
    
    
    
}