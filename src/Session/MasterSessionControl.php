<?php


namespace Phr\Griffin\Session;

use Phr\Griffin\GriffinException;
use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\GriffinBase\Errors as ERR;
use Phr\Griffin\Service\SessionService;
use Phr\Griffin\Session\SessionBase\SessionModel;
use Phr\Griffin\Entity\Users;
use Phr\Griffin\Tokens\SessionJwt;
use Phr\Griffin\Tokens\Settings;
use Phr\Eojwt\Accounts\SessionAccount;
use Phr\Eojwt\Accounts\UserAccount;
use Phr\Eojwt\Accounts\SessionFp;
use Phr\Eojwt\Accounts\ActiveSession;

use Phr\Eojwt\EoJwtBase\TokenType;
use Phr\Griffin\Tokens\RefreshJwt;
use Phr\Eojwt\EoJwt;
use Phr\Eojwt\Accounts\UserRols;
use Phr\Griffin\Contracts\Login\LoginResponse;
use Phr\Griffin\Contracts\Login\AccountTokenResponse;
use Phr\Webapi\ApiControl\Authorization\Sessions\SessionControl as ApiSession;
use Phr\Griffin\Session\SessionBase\UserSession;
use Phr\Griffin\Tokens\SecureAccountJwt;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Certificator\Encryption;
use Phr\Eojwt\EoJwtException;

use Phr\Griffin\GriffinBase\Support\SupportFiles;

use Phr\Griffin\Session\SessionBase\CertSession;

class MasterSessionControl extends SessionModel
{   
    readonly private SessionService $service;

    public function __construct()
    {
        parent::__construct(
            SHELL::masterId(),
            SHELL::MASTER_REALM,
            SHELL::MASTER_CLIENT
        );
    }


    public function createNewMasterSession(string $_username): LoginResponse
    {   
        $key = md5(rand());
        $userSession = new UserSession($this->userId, $_username, new UserRols(['admin']));

        $newMasterSession = new CertSession($userSession->sessionAccout());

        $newMasterSession->createSession($key);

        $jwtGenerator = new RefreshJwt(new Settings(
            TokenType::REFJWT
            ,$this->issuer
            ,$this->realmId
            ,$this->clientId 
            ,$key
            ,null
            ,null
            ,10
        ));
        
        $jwtGenerator->generate(new ActiveSession($this->userId, $userSession->sessionFp()));
        $jwtGenerator->sign($this->publicRsa);
        $token = $jwtGenerator->token();
        
        return new LoginResponse($token);
    }
    /**
     * 
     * @method validate master refresh token
     * 
     */
    public function validate()
    {   
        /**
         * Intercept refresh token
         */
        $refreshToken = SHELL::authorization();
        if(!$refreshToken) throw new GriffinException(ERR::E5030000, 'refresh token');

        try{
            $jwt = new EoJwt($refreshToken);
            $session = new SessionControl(
                $jwt->payload()->session->accountId,
                $jwt->payload()->realmId,
                $jwt->payload()->clientId
            );
            $jwt->validate($this->privateRsa, false);
            $decryptor = $jwt->getSessionFile();

        }catch(EoJwtException $error)
        {
            throw new GriffinException(ERR::E5030005, $error->getMessage());
        }
        /**
         * Retrive master certificate.
         */
        $certContent = SHELL::retriveSessionFile(SHELL::masterId(), $decryptor);
        $fpControl = $jwt->payload()->session->sessionFp;
        /**
         * Match control fingerprints.
         */
        if($certContent->content->sessionFingerprints->fp1 !== $fpControl->fp1) throw new GriffinException(ERR::E5030005);
        if($certContent->content->sessionFingerprints->fp2 !== $fpControl->fp2) throw new GriffinException(ERR::E5030005);
        if($certContent->content->sessionFingerprints->fp3 !== $fpControl->fp3) throw new GriffinException(ERR::E5030005);
        if($certContent->content->sessionFingerprints->fp4 !== $fpControl->fp4) throw new GriffinException(ERR::E5030005);
        if($certContent->content->sessionFingerprints->fp5 !== $fpControl->fp5) throw new GriffinException(ERR::E5030005);
        /**
         * Retrive master user data. 
         */
        $masterData = SHELL::retriveGriffinFile(SupportFiles::IDP_MASTER_CERT, SHELL::masterEncryptor());
        $this->username = $masterData->content->username;
        /**
         * Reastablish session account.
         */
        $this->session = new SessionAccount(
            $certContent->content->userId,
            $certContent->content->sessionId,
            $certContent->content->sessionTs,
            $certContent->content->expire,
            $certContent->content->enryptor,
            $certContent->content->sessionIv,
            new SessionFp(
                [
                    $certContent->content->sessionFingerprints->fp1,
                    $certContent->content->sessionFingerprints->fp2,
                    $certContent->content->sessionFingerprints->fp3,
                    $certContent->content->sessionFingerprints->fp4,
                    $certContent->content->sessionFingerprints->fp5,
                ]
            )
        );
        
        $this->sessionId = $certContent->content->sessionId;
       
        return true;

    }
    public function decryptPost(): void
    {   
        $decryptedKey = Encryption::sslPrivateDecrypt($this->privateRsa, self::$sessionKey);
        $postedHex = SHELL::incomeing();
        $postedEncrypted = Encryption::decodeHex($postedHex);
        $decrypt = new Encryption($decryptedKey);
        SHELL::setDecryptedIncomeing($decrypt->fernetDecrypt($postedEncrypted));
    }
    /**
     * @method create master info token.
     * @return AccountTokenResponse
     */
    public function masterInfoToken(): AccountTokenResponse
    {   
        $jwtGenerator = new SecureAccountJwt(new Settings(
            TokenType::SECJWT
            ,$this->issuer
            ,$this->realmId
            ,$this->clientId 
            ,$this->decryptProcessKey()
            ,null
            ,null
            ,10
        ));

        $jwtGenerator->generate($this->userAccout("ADMIN"));
        $jwtGenerator->sign($this->privateRsa);
        $token = $jwtGenerator->token();

        return new AccountTokenResponse($token);
    }
    
    
    
}