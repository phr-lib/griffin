<?php

namespace Phr\Griffin\Session\SessionBase;

use Phr\Eojwt\Accounts\SessionAccount;
use Phr\Eojwt\Accounts\UserAccount;
use Phr\Eojwt\Accounts\UserRols;
use Phr\Certificator\Crips;
use Phr\Eojwt\Accounts\SessionFp;
use Phr\Certificator\Encryption;


class UserSession extends SessionAccount
{   
    private string $username;

    private UserRols $userRools;

    public function __construct(
        string $_user_id,
        string $_username,
        UserRols $_user_rols,
        int $_expire = 3600
    ){   
        $ts = time();
        $this->username = $_username;
        $this->userRools = $_user_rols;

        parent::__construct(
            $_user_id,
            Crips::generateUniqueKeyId(),
            $ts,
            $ts + $_expire,
            md5(rand()),
            $this->sessionIv(16),
            new SessionFp($this->createFingerPrints())
        );
    }

    private function sessionIv($digits) {
        $temp = "";
      
        for ($i = 0; $i < $digits; $i++) {
          $temp .= rand(0, 9);
        }
      
        return (string)$temp;
    }
    private function createFingerPrints(): array
    {
        $fp = [];
        for($x = 0; $x<5; $x++)
        {
            array_push($fp, Crips::generateFingerPrint());
        }
        return $fp;
    }
    public function sessionAccout(): SessionAccount
    {
        return new SessionAccount(
            $this->userId,
            $this->sessionId,
            $this->sessionTs,
            $this->expire,
            $this->enryptor,
            $this->sessionIv,
            new SessionFp(
                [
                    $this->sessionFingerprints->fp1,
                    $this->sessionFingerprints->fp2,
                    $this->sessionFingerprints->fp3,
                    $this->sessionFingerprints->fp4,
                    $this->sessionFingerprints->fp5,
                ]
            )
        );
    }
    public function userAccout(): UserAccount
    {
        return new UserAccount(
            $this->userId,
            $this->username,
            $this->sessionTs,
            $this->enryptor,
            $this->sessionIv,
            $this->userRools
        );
    }
    public function sessionFp(): SessionFp
    {
        return $this->sessionFingerprints;
    }
    public function sessionToken(): string
    {
        return Encryption::baseEncode(json_encode($this->sessionFingerprints));
    }
}