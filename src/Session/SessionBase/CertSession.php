<?php

namespace Phr\Griffin\Session\SessionBase;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Griffin\GriffinException;
use Phr\Griffin\GriffinBase\Errors as ERR;

use Phr\Certificator\SaveFile;
use Phr\Certificator\FileHandler\FileSettings;
use Phr\Certificator\SaveFileError;

use Phr\Webapi\ApiBase\Support\SupportFiles as SF;
use Phr\Eojwt\Accounts\SessionAccount;

/**
 * 
 * Create master session
 * certificate.
 * 
 */
final class CertSession
{   
    # /// CONSTRUCTOR ***
    public function __construct(
        readonly private SessionAccount $session
    ){}

    /**
     * 
     * 
     * @method creates user certificate. 
     * @param string key
     * 
     */
    public function createSession(string $_key): void
    {   
        try 
        {
            $sessionCert = new SaveFile(SHELL::projectFolder().SF::SESSION_CERT->folder(), 
            new FileSettings(   SF::SESSION_CERT->fileType()
                                ,SHELL::settings()->solutionId
                                ,SHELL::settings()->applicationId
                                ,$_key
                            )
            );
            
            $sessionCert->createCertificate($this->session, $this->session->userId);

        }catch(SaveFileError $error)
        {
            throw new GriffinException(ERR::E5030000, 'Master session cert');
        }
    }
}