<?php


namespace Phr\Griffin\Session\SessionBase;

use Phr\Griffin\GriffinBase\GriffinShell as SHELL;
use Phr\Webapi\ApiTools\ApiSecure;
use Phr\Eojwt\Accounts\SessionAccount;
use Phr\Eojwt\Accounts\UserAccount;
use Phr\Eojwt\Accounts\UserRols;
use Phr\Certificator\Encryption;

abstract class SessionModel 
{   
    readonly protected string $userId;

    readonly protected string $issuer;

    readonly protected string $realmId;

    protected string $clientId;

    protected string $publicRsa;

    protected string $privateRsa;

    protected SessionAccount $session;

    protected static string $sessionKey;

    protected string $username;

    protected string $sessionId;


    public static function setSessionKey(string $_session_key): void 
    {
        self::$sessionKey = $_session_key;
    }
    public function decryptProcessKey(): string|null
    {
        return Encryption::sslPrivateDecrypt($this->privateRsa, self::$sessionKey);
    }
    public function getPrivate(): string { return $this->privateRsa; }

    public function __construct(string $_user_id, string $_realm_id, string $_client_id)
    {   
        $this->userId = $_user_id;
        $this->issuer = $_SERVER['SERVER_ADDR'];
        $this->realmId = $_realm_id;
        $this->clientId = $_client_id;
        $this->setKeys();
    }
    private function setKeys()
    {  
        if($this->realmId == 'masterRealm')
        {
            $this->publicRsa =  ApiSecure::retrivePublicRsa();
            $this->privateRsa =  ApiSecure::retrivePrivateRsa();
        }else
        { 
            $this->privateRsa =  SHELL::getRealmPrivate($this->realmId);  
        }
    }
    public function userAccout(): UserAccount
    {   
        return new UserAccount(
            $this->session->userId,       
            $this->realmId,
            $this->username,       
            $this->sessionId,
            $this->session->enryptor,
            $this->session->sessionIv,
            new UserRols(['admin'])
        );
    }
}