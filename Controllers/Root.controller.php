<?php

namespace Controllers;

use Phr\Webapi as Web;
use Phr\Webapi\ApiControl as I;
use Phr\Webapi\ApiControl\ResponseCode as RC;
use Phr\Webapi\ApiControl\Authenticate;
use Phr\Webapi\ApiControl\Contracts;


/**
 * @final ROOT CONTROLLER
 */
final class Root extends Web\Controller implements Web\POST, Web\GET, Web\PATCH
{  
    /// CONTROLLER ***
    public function controller(): void
    {   
        switch ( self::method() ) 
        {
            case self::GET: $this->GET(); break;

            case self::POST: $this->POST(); break;

            case self::UPDATE: $this->UPDATE(); break;

            case self::PATCH: $this->PATCH(); break;       
        }     
    }
    /// GET method
    public function GET(): void
    {   
        self::response(RC::OK, Web\Api::healthCheck());
    }
    /// POST method
    public function POST(): void
    {   
        #I\Authenticate::header();
        // POSTED data
        $posted = self::posted();
        self::registerApp(self::posted());
        self::response(self::OK, $posted);       
    }
    public function UPDATE(): void
    {   
        self::response(RC::OK, Web\Api::updateRegistration()); 
    }
    public function PATCH(): void
    {   
        self::response(RC::CREATED, Web\Api::register());   
    }
}